import { PushNotificationState } from ".";

interface ApplicationState {
  pushNotifications: PushNotificationState
}

export const deviceTokenSelector = (state: ApplicationState) => ({
  deviceToken: state.pushNotifications.deviceToken
});

export const openedInitialNotificationSelector = (state: ApplicationState): boolean => {
  return state.pushNotifications.openedInitialNotification
}