import {
  all,
  call,
  cancelled,
  fork,
  put,
  take,
  takeLatest,
  select
} from "redux-saga/effects";

import { Notifications } from "react-native-notifications";

import {
  PUSH_NOTIFICATIONS_REGISTERED,
  PUSH_NOTIFICATIONS_START,
  NotificationsRegisteredAction,
  openedNotification,
  openedInitialNotification,
  loadedToken,
} from "./actions";

import createNotificationEventsChannel from "./notification_events_channel";
import { openedInitialNotificationSelector } from "./selectors";

// Ask for notificatino permissions if required 
export function* registerForNotifications() {
  Notifications.registerRemoteNotifications();

  const openedInitialAlready = yield select(openedInitialNotificationSelector);
  if (openedInitialAlready) return;

  try { 
    // Try get notifications that caused us to launch
    const notification = yield call([Notifications, Notifications.getInitialNotification])
    if (notification) {
      yield put(openedNotification(notification))
      yield put(openedInitialNotification())
    }
  } catch(err) {
    console.error("getInitialNotifiation() failed", err);
  }
}

// Start a channel that will emit events from `react-native-notifications`
export function* watchPushNotificationEvents() {
  // Load the previous token from async storage
  const deviceToken = yield call(config.loadToken)

  // Update the store with the token
  yield put(loadedToken(deviceToken));

  // Create the events channel
  // This will forward us actions when react-native-notification events occur
  const eventChannel = yield call(createNotificationEventsChannel);

  try {
    while (true) {
      // Keep tacking events from the channel
      const action = yield take(eventChannel);
      yield put(action);
    }
  } finally {
    // Close the channel when the saga is cancelled
    if (yield cancelled()) {
      eventChannel.close();
    }
  }
}

// Received a new device token from react-native-notifications
export function* registeredDeviceToken(action: NotificationsRegisteredAction) {
  // Save the token
  yield call(config.saveToken, action.payload.deviceToken)

  // Update the store with the token
  yield put(loadedToken(action.payload.deviceToken));
}

interface SagaConfiguration {
  loadToken: () => Promise<string | null>
  saveToken: (deviceToken: string) => Promise<void>
}

let config: SagaConfiguration

let createSaga = (configuration: SagaConfiguration) => {
  config = configuration
  return all([
    // Start registering for push notifications
    takeLatest(PUSH_NOTIFICATIONS_START, registerForNotifications),

    // Push token registered
    takeLatest(PUSH_NOTIFICATIONS_REGISTERED, registeredDeviceToken),

    // Watch for notification events
    fork(watchPushNotificationEvents),
  ]);
}

export { createSaga }