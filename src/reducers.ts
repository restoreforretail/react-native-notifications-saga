import { PUSH_NOTIFICATIONS_LOADED_TOKEN, PushNotificationAction, PUSH_NOTIFICATIONS_REGISTRATION_FAILED, PUSH_NOTIFICATIONS_OPENED_INITIAL_NOTIFICATION } from './actions';
import { RegistrationError } from 'react-native-notifications';

export interface PushNotificationState {
  deviceToken?: string
  registrationError?: RegistrationError
  openedInitialNotification: boolean
}

const initialState: PushNotificationState = {
  deviceToken: undefined,
  registrationError: undefined,
  openedInitialNotification: false
}

export default (state = initialState, action: PushNotificationAction) => {
  switch (action.type) {
    // We loaded a push token from async storage
    case PUSH_NOTIFICATIONS_LOADED_TOKEN:
      return {
        ...state,
        deviceToken: action.payload.deviceToken
      }
    case PUSH_NOTIFICATIONS_REGISTRATION_FAILED:
      return {
        ...state,
        registrationError: action.payload.error
      }
      case PUSH_NOTIFICATIONS_OPENED_INITIAL_NOTIFICATION:
        return {
          ...state,
          openedInitialNotification: true
        }
    }
    return state
}
