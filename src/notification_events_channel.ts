import { eventChannel } from "redux-saga";
import { Notifications, RegistrationError, Notification, NotificationResponse } from "react-native-notifications";
import { registeredNotifications, PushNotificationAction, openedNotification, registrationFailed } from "./actions";
import { NotificationCompletion } from "react-native-notifications/lib/dist/interfaces/NotificationCompletion";

// Creates a channel that can emit events from `react-native-notifications`
// We evemit events as redux actions 
export default function createNotificationEventsChannel() {
  return eventChannel<PushNotificationAction>(emitter => {
    //
    // Subscribe to events
    //
    let registerEvent = Notifications.events().registerRemoteNotificationsRegistered(
      event => {
        emitter(registeredNotifications(event.deviceToken));
      }
    );

    let registerFailedEvent = Notifications.events().registerRemoteNotificationsRegistrationFailed(
      (event: RegistrationError) => {
        emitter(registrationFailed(event))
      }
    );

    let receivedEvent = Notifications.events().registerNotificationReceivedForeground(
      (
        notification: Notification,
        completion: (response: NotificationCompletion) => void
      ) => {
        let nc: NotificationCompletion = {
          alert: true,
          sound: true,
        };
        completion(nc);
      }
    );

    let receivedBackgroundEvent = Notifications.events().registerNotificationReceivedBackground(
      (
        notification: Notification,
        completion: (response: NotificationCompletion) => void
      ) => {
        let nc: NotificationCompletion = {
          alert: true,
          sound: true,
        };
        completion(nc);
      }
    );

    let openedEvent = Notifications.events().registerNotificationOpened(
      (response: NotificationResponse, completion: () => void) => {
        // Response is actually `Notification` not `NotifiationResponse`
        // As of 6/2/2020 this has not been fixed but someone has fixed it here: 
        // https://github.com/wix/react-native-notifications/issues/469
        emitter(openedNotification(response as any as Notification));
        completion();
      }
    );


    // Channel close callback
    return () => {
      // Unsubscribe to the events
      registerEvent.remove();
      registerFailedEvent.remove();
      receivedEvent.remove();
      receivedBackgroundEvent.remove();
      openedEvent.remove();
    };
  });
}