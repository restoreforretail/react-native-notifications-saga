import { Action, ActionCreator } from "redux";
import { Notification, RegistrationError } from "react-native-notifications";

export const PUSH_NOTIFICATIONS_START = "PUSH_NOTIFICATIONS_START";
export const PUSH_NOTIFICATIONS_REGISTERED = 'PUSH_NOTIFICATIONS_REGISTERED';
export const PUSH_NOTIFICATIONS_REGISTRATION_FAILED = 'PUSH_NOTIFICATIONS_REGISTRATION_FAILED';
export const PUSH_NOTIFICATIONS_LOADED_TOKEN = 'PUSH_NOTIFICATIONS_LOADED_TOKEN';
export const PUSH_NOTIFICATIONS_OPENED_NOTIFICATION = 'PUSH_NOTIFICATIONS_OPENED_NOTIFICATION';
export const PUSH_NOTIFICATIONS_OPENED_INITIAL_NOTIFICATION = 'PUSH_NOTIFICATIONS_OPENED_INITIAL_NOTIFICATION'

export interface NotificationsRegisteredAction extends Action {
  type: typeof PUSH_NOTIFICATIONS_REGISTERED,
  payload: {
    deviceToken: string
  }
}

export interface RegistrationFailedAction extends Action {
  type: typeof PUSH_NOTIFICATIONS_REGISTRATION_FAILED,
  payload: {
    error: RegistrationError
  }
}

export interface StartPushNotificationsAction extends Action {
  type: typeof PUSH_NOTIFICATIONS_START,
}

export interface LoadTokenAction extends Action {
  type: typeof PUSH_NOTIFICATIONS_LOADED_TOKEN,
  payload: {
    deviceToken?: string
  }
}

export interface OpenedNotificationAction extends Action {
  type: typeof PUSH_NOTIFICATIONS_OPENED_NOTIFICATION, 
  payload: {
    notification: Notification
  }
}

export interface OpenedInitialNotificationAction extends Action {
  type: typeof PUSH_NOTIFICATIONS_OPENED_INITIAL_NOTIFICATION,
}

export type PushNotificationAction = NotificationsRegisteredAction | RegistrationFailedAction | StartPushNotificationsAction | LoadTokenAction | OpenedNotificationAction | OpenedInitialNotificationAction

export const registeredNotifications: ActionCreator<NotificationsRegisteredAction> = (deviceToken: string) => ({
  type: PUSH_NOTIFICATIONS_REGISTERED,
  payload: { deviceToken }
})

export const loadedToken: ActionCreator<LoadTokenAction> = (deviceToken?: string) => ({
  type: PUSH_NOTIFICATIONS_LOADED_TOKEN,
  payload: { deviceToken }
})

export const startPushNotifications: ActionCreator<StartPushNotificationsAction> = () => ({
  type: PUSH_NOTIFICATIONS_START
})

export const registrationFailed: ActionCreator<RegistrationFailedAction> = (error: RegistrationError) => ({
  type: PUSH_NOTIFICATIONS_REGISTRATION_FAILED,
  payload: { 
    error
  }
})

export const openedNotification: ActionCreator<OpenedNotificationAction> = (notification: Notification) => ({
  type: PUSH_NOTIFICATIONS_OPENED_NOTIFICATION,
  payload: { notification }
})

export const openedInitialNotification: ActionCreator<OpenedInitialNotificationAction> = () => ({
  type: PUSH_NOTIFICATIONS_OPENED_INITIAL_NOTIFICATION
})